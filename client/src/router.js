import Vue from 'vue';
import Router from 'vue-router';
import VueAnalytics from 'vue-analytics'

import FeedPage from './views/Feed.vue';
import LoginPage from './views/Login.vue';
import PanelPage from './views/panel/Panel.vue';
import CreatePostPage from './views/panel/CreatePost.vue';
import UpdatePostPage from './views/panel/UpdatePost.vue';
import PostPage from './views/Post.vue';
import NotFoundPage from './views/PageNotFound.vue';

Vue.use(Router);

const router = new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            name: 'home',
            component: FeedPage,
            meta: {
                title: 'Ana Səhifə - Nicat Məmmədov',
                metaTags: [
                    {
                        name: 'robots',
                        content: 'index,follow'
                    },
                ]
            },
        },
        {
            path: '/oxu/:id',
            name: 'post',
            meta: {
                metaTags: [
                    {
                        name: 'robots',
                        content: 'index,follow'
                    },
                    {
                        name: 'author',
                        content: 'Nicat Məmmədov'
                    },
                    {
                        name: 'keywords',
                        content: ''
                    },
                    {
                        property: 'og:type',
                        content: 'article'
                    },
                    {
                        property: 'og:locale',
                        content: 'az-AZ'
                    },
                ]
            },
            component: PostPage
        },
        {
            path: '/login',
            name: 'login',
            component: LoginPage,
            meta: {
                title: 'Daxil ol',
                metaTags: [
                    {
                        name: 'robots',
                        content: 'noindex,nofollow'
                    },
                ]
            }
        },
        {
            path: '/panel',
            name: 'panel',
            meta: {
                title: 'Panel',
                metaTags: [
                    {
                        name: 'robots',
                        content: 'noindex,nofollow'
                    },
                ]
            },
            component: PanelPage
        },
        {
            path: '/panel/createPost',
            name: 'CreatePost',
            meta: {
                title: 'Yeni məqalə',
                metaTags: [
                    {
                        name: 'robots',
                        content: 'noindex,nofollow'
                    },
                ]
            },
            component: CreatePostPage
        },
        {
            path: '/panel/updatePost/:id',
            name: 'UpdatePost',
            meta: {
                title: 'Məqaləyə düzəliş et',
                metaTags: [
                    {
                        name: 'robots',
                        content: 'noindex,nofollow'
                    },
                ]
            },
            component: UpdatePostPage
        },
        {
            path: '*',
            name: 'PageNotFound',
            component: NotFoundPage,
            meta: {
                title: 'Axtardığınız səhifə mövcud deyil',
                metaTags: [
                    {
                        name: 'robots',
                        content: 'noindex,nofollow'
                    },
                ]
            },
        },
    ],
});
// console.log(router.options.routes);
router.afterEach((to, from) => {

    // This goes through the matched routes from last to first, finding the closest route with a title.
    // eg. if we have /some/deep/nested/route and /some, /deep, and /nested have titles, nested's will be chosen.
    const nearestWithTitle = to.matched.slice().reverse().find(r => r.meta && r.meta.title);

    // Find the nearest route element with meta tags.
    const nearestWithMeta = to.matched.slice().reverse().find(r => r.meta && r.meta.metaTags);
    // const previousNearestWithMeta = from.matched.slice().reverse().find(r => r.meta && r.meta.metaTags);

    // If a route with a title was found, set the document (page) title to that value.
    if(nearestWithTitle) document.title = nearestWithTitle.meta.title;

    // Remove any stale meta tags from the document using the key attribute we set below.
    Array.from(document.querySelectorAll('[data-vue-router-controlled]')).map(el => el.parentNode.removeChild(el));

    // Skip rendering meta tags if there are none.
    if(!nearestWithMeta) return;

    // Turn the meta tag definitions into actual elements in the head.
    nearestWithMeta.meta.metaTags.map(tagDef => {
        const tag = document.createElement('meta');

        Object.keys(tagDef).forEach(key => {
            tag.setAttribute(key, tagDef[key]);
        });

        // We use this to track which meta tags we create, so we don't interfere with other ones.
        tag.setAttribute('data-vue-router-controlled', '');

        return tag;
    })
    // Add the meta tags to the document head.
        .forEach(tag => document.head.appendChild(tag));
});

/*Vue.use(VueAnalytics, {
    id: 'UA-144556377-1',
    router
});*/

export default router;
