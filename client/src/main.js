import Vue from 'vue';
import axios from 'axios';
// import Vuetify from 'vuetify';
import App from './App.vue';
import router from './router';
import VueDisqus from 'vue-disqus';
// import 'vuetify/dist/vuetify.min.css';
import vuetify from './plugins/vuetify';

Vue.config.productionTip = false;

window.EventBus = new Vue();
window.axios = axios;
window.axios.defaults.withCredentials = true;
window.FULL_PATH = 'http://127.0.0.1:3000';
window.API_URL = FULL_PATH+'/api';

// Vue.use(Vuetify);
Vue.use(VueDisqus);

new Vue({
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app');
