'use strict';

//region Constants
// const createError = require('http-errors');
const express = require('express');
const app = express();
const path = require('path');
const cors = require('cors');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const fs = require('fs');
const session = require('express-session')({
  name: 'sessionName',
  secret: 'keyboard cat',
  resave: true,
  saveUninitialized: true,
  cookie: {
    maxAge: 60 * 1000 * 60 * 6,
    resave: false,
    saveUninitialized: true
  }
});
//endregion

if (app.get('env') === 'production') {
  app.set('trust proxy', 1); // trust first proxy
}

//region view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');
//endregion

//region using app tools
app.use(logger('common', {
  stream: fs.createWriteStream('./server.log', {flags: 'a'})
}));
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(session);
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors({ //this is unnecessary code on production
  origin: 'http://127.0.0.1:8080',
  methods: [/*'GET',*/'POST'],
  credentials: true // enable set cookie
}));
//endregion

app.use('/', require('./routes/index'));
app.use('/api', require('./routes/api'));
app.use('/api/panel', function (req, res, next){
  if(typeof req.session.user === 'undefined') {
    res.json({status: 'denied', message: 'No session detected!'});
    return;
  }
  else next();
}, require('./routes/panel'));

//region error handlers
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  res.render('index');
  // next(createError(404)); //single page olduğu üçün 404 səhifəsi əvəzinə həmişə index render olunmalıdır
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});
//endregion

module.exports = app;
