'use strict';

const express = require('express');
const PostModel = require('../model/posts');
const UserModel = require('../model/user');

const router = express.Router();
const postModel = new PostModel();
const userModel = new UserModel();

router.post('/getPosts', function(req, res, next) {
  postModel.getFeed()
      .catch(e => {
        res.json([]); //TODO: create error log
      })
      .then(r => {
        res.json(r);
      });
});

router.post('/getPostByID', function(req, res, next) {
  postModel.getPostByID(req.body.id)
      .catch(e => {
        res.json([]); //TODO: create error log
      })
      .then(r => {
        res.json(r[0]);
      });
});

router.post('/login', function(req, res, next) {
    if(typeof req.body.email === 'undefined' ||
       typeof req.body.password === 'undefined'){

        res.json({
            status: 'failed',
            reason: 'missing_params',
            message: 'Missing params.'
        });

        return;
    }

    userModel.login(req.body.email, req.body.password)
        .then(r => {
            if(r.length === 0){
                res.json({
                    status: 'failed',
                    reason: 'bad_credentials',
                    message: 'Wrong email or password.'
                });
                return;
            }

            r = r[0];

            req.session.user = {
                id: r.id,
                email: r.email,
                fullName: r.fullname
            };

            res.json({
                status: 'success',
                user: req.session.user
            });
        })
        .catch(e => {
            res.json({
                status: 'failed',
                reason: 'server_side_error',
                message: 'Unknown error occurred on database.'
            });
        });
});

router.post('/authenticate', function(req, res, next) {
    if(typeof req.session.user === 'undefined') {
        res.json({status: 'denied', message: 'No session detected!'});
        return;
    }

    res.json({
        status: 'success',
        session: req.session.user
    });
});

module.exports = router;
