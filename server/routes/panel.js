'use strict';

const express = require('express');
const PanelModel = require('../model/panel');

const router = express.Router();
const panelModel = new PanelModel();

router.post('/getCategories', function(req, res, next) {
    panelModel.getCategories()
        .catch(e => {
            res.json([]); //TODO: create error log
        })
        .then(r => {
            res.json(r);
        });
});

router.post('/createPost', function(req, res, next) {
    panelModel.createPost(req.body)
        .catch(e => {
            res.json({status: 'failed'}); //TODO: create error log
        })
        .then(r => {
            if(r.affectedRows > 0)
                res.json({status: 'success'});
            else res.json({status: 'failed'});
        });
});

router.post('/updatePost', function(req, res, next) {

    panelModel.updatePost(req.body.id, req.body.data)
        .catch(e => {
            res.json({status: 'failed'});
        })
        .then(r => {
            if(r.affectedRows > 0)
                res.json({status: 'success'});
            else res.json({status: 'failed'});
        });
});

router.post('/deletePost', function(req, res) {

    panelModel.deletePost(req.body.id)
        .catch(e => {
            res.json({status: 'failed'});
        })
        .then(r => {
            if(r.affectedRows > 0)
                res.json({status: 'success'});
            else res.json({status: 'failed'});
        });
});

router.post('/getPost', function(req, res, next) {
    panelModel.getPost(req.body.id)
        .catch(e => res.json({status: 'failed'}))
        .then(r => {
            if(r.length > 0) res.json({status: 'success', post: r[0]});
            else res.json({status: 'failed'});
        });
});

router.post('/getAllPosts', function (req, res) {
    panelModel.getAllPosts()
        .catch(e => {
            res.json([]);
        })
        .then(r => {
            res.json(r);
        });
});

module.exports = router;
